describe('Testing for Upload Page flow', () => {
    beforeEach(() => {
        cy.reload
    })

    it('Unauthorized user cannot acces and will be redirected to /login', () => {
        cy.visit('http://localhost:3000/profile/upload')
        cy.url().should('include', '/login')
    })

    it('if user not found will error 404', () => {
        cy.setCookie("userId", "123");
        cy.visit('http://localhost:3000/profile/upload')
        cy.contains('Choose Image').click().selectFile('cypress/images/batu.png')
        cy.contains('Update', {timeout: 10000}).click()
        cy.contains('Update photo failed..', {timeout: 15000})
    })

    it('if image invalid will error 409', () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
        cy.visit('http://localhost:3000/profile/upload')
        cy.contains('Choose Image').click().selectFile('cypress/images/file.json')
    })

    it('Upload Page can be accessed with authorized user', () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");

        cy.visit('http://localhost:3000/profile/upload')
        cy.url().should('include', '/upload')
    })


    it("User can upload photo", () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
        cy.visit('http://localhost:3000/profile/upload')
        cy.contains('Choose Image').click().selectFile('cypress/images/batu.png')
        cy.contains('Update', {timeout: 10000}).click()
        cy.contains('Your profile photo is updated !', {timeout: 15000})
    });


})