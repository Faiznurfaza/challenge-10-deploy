describe("Test for GamePlay Page flow", () => {
    beforeEach(() => {
        cy.reload
    })
    it('Game 1 can be visited with authorized user', () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
        cy.visit('http://localhost:3000/games/1/play')
    })

    it('Game 2 can be visited with authorized user', () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
        cy.visit('http://localhost:3000/games/2/play')
    })

    it('Dummy can be visited with authorized user', () => {
        cy.setCookie("userId", "7475cb77-71e0-49ba-902f-ddb079f75750");
        cy.visit('http://localhost:3000/games/3/play')
        cy.visit('http://localhost:3000/games/4/play')
        
    })

    it('Game 1 cannot be accesed with unauthorized user (must login or register first)', () => {
        cy.visit('http://localhost:3000/games/1/play')
        cy.url().should('include', '/register')
    })

    it('Game 2 cannot be accesed with unauthorized user (must login or register first)', () => {
        cy.visit('http://localhost:3000/games/2/play')
        cy.url().should('include', '/register')
    })

    it('Dummy game cannot be accesed with unauthorized user (must login or register first)', () => {
        cy.visit('http://localhost:3000/games/3/play')
        cy.visit('http://localhost:3000/games/4/play')
        cy.url().should('include', '/register')
    })

})