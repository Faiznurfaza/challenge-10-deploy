### FRONT-END TESTING REPOSITORY FOR CH 11
##Step By Step for testing & running this app
1. Clone this project.
2. Run `npm install`.
3. After that, Open 2 terminal then you can `npm run dev`, give it a time to load everything
4. Then one terminal run `npm run cypress`
5. If you run all the tests, it should took roughly 8 mins to complete

###Note
- If some of tests are failed, consider to rerun the test
- If you want to check the deployment repository, please go to https://gitlab.com/Faiznurfaza/gatotsprinter-fe