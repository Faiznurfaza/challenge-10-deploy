"use client";

import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import axios from "axios";
import LogoutPopup from "./LogoutPopup";
import { useRouter } from "next/navigation";
import { LogoutAction } from "@/redux/features/AuthReducer";
import { useAppSelector, useAppDispatch } from "@/redux/hooks";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

export default function Logout() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const [logoutButton, setLogoutButton] = useState("Logout");
  const [spinner, setSpinner] = useState("");
  useEffect(() => {
    const userId = Cookies.get("userId");
    if (!userId) {
      router.push("/login");
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const handleLogout = async (e) => {
    e.preventDefault();
    try {
      setLogoutButton("");
      setSpinner(faSpinner);
      const res = await axios.post(
        "https://gatots-api.vercel.app/api/user/logout"
      );
      Cookies.remove("userId");

      setSpinner("");
      setLogoutButton("Done");
      router.push("/login");
      dispatch(LogoutAction());
    } catch (err) {
      console.log(err);

      setSpinner("");
      setLogoutButton("Logout failed");

      setTimeout(() => {
        setLogoutButton("Logout");
      }, 1000);
    }
  };

  const handleCancel = (e) => {
    e.preventDefault();
    router.push("/home");
  };

  return (
    <div>
      <LogoutPopup
        handleLogout={handleLogout}
        handleCancel={handleCancel}
        logoutButton={logoutButton}
        Spinner={spinner}
      />
    </div>
  );
}
