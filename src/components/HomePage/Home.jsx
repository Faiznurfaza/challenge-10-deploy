"use client";

import Image from "next/image";
import Link from "next/link";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faCircleExclamation,
} from "@fortawesome/free-solid-svg-icons";

function Home({ user, initials }) {
  return (
    <div
      className="flex flex-col items-center h-screen bg-cover bg-center absolute inset-0"
      style={{
        backgroundImage: `url('/assets/main-bg.jpg')`,
        position: "relative",
        zIndex: 0,
      }}
    >
      <h1 className="text-white font-bold text-xl mt-2 text-center">
        Welcome, {user?.username}{" "}
      </h1>
      <div
        style={styles.container}
        className="max-w-sm w-full lg:max-w-full lg:flex mt-8"
      >
        <div
          style={styles.border}
          className="w-1/2 bg-white p-9 rounded-xl bg-opacity-60 backdrop-filter backdrop-blur-lg flex flex-col justify-between leading-normal"
        >
          {!user?.photo ? (
            <div className="profile-photo w-32 h-32 rounded-full mx-auto flex items-center justify-center bg-gray-500 text-white text-6xl font-bold">
              {initials}
            </div>
          ) : (
            <div className="profile-photo">
              <Image
                src={user.photo}
                alt="Profile picture"
                width={80}
                height={80}
                style={styles.imageStyle}
                quality={100}
              />
            </div>
          )}
          <div className="mb-8">
            <p className="text-black text-base font-bold text-center">User Info</p>
            <p className="text-black text-base">Username: {user?.username}</p>
            <p className="text-black text-base">Email: {user?.email}</p>
          </div>
          <div className="flex items-center">
            <div className="bg-transparent hover:bg-gray-500 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-500 hover:border-transparent rounded">
              <Link href="./profile" className="view-profile leading-none">
                View My Profile
              </Link>
            </div>
          </div>
        </div>

        <div
          style={styles.border}
          className="w-1/2 bg-white p-9 rounded-xl bg-opacity-60 backdrop-filter backdrop-blur-lg flex flex-col justify-between leading-normal text-center"
        >
          <h2 className="text-2xl font-bold mb-4 text-black">
            Whats New? <FontAwesomeIcon icon={faCircleExclamation} />
          </h2>
          <p className="text-base">User can change their profile picture</p>
          <Link
            href="/profile/upload"
            className="text-base font-semibold mt-2 text-blue-500 hover:text-blue-800"
          >
            <FontAwesomeIcon icon={faArrowRight} /> Change here
          </Link>

          <p className="text-base mt-4">User can upload & stream videos</p>
          <Link
            href="/video"
            className="text-base font-semibold mt-2 text-blue-500 hover:text-blue-800"
          >
            <FontAwesomeIcon icon={faArrowRight} /> More Info
          </Link>
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
  },
  border: {
    width: "350px",
    height: "350px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    margin: "0 16px",
  },
  imageStyle: {
    borderRadius: `9999px`,
    marginLeft: "auto",
    marginRight: "auto",
    objectFit: "cover",
  },
};

export default Home;
