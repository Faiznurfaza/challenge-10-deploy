"use client";

import React from "react";
import Cookies from "js-cookie";
import axios from "axios";
import { useRouter } from "next/navigation";

function DummyGame() {
  let userId = Cookies.get("userId");
  const router = useRouter();
  const getScore = async () => {
    try {
      if (userId !== undefined) {
        await axios.post(
          `https://gatots-api.vercel.app/api/v1/score/add/${userId}/3`
        );
        await axios.post(
          `https://gatots-api.vercel.app/api/v1/score/add/${userId}/4`
        );
      } else {
        router.push("/login");
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <>
      <div className="flex flex-col items-center justify-center min-h-screen bg-blue-200">
        <div className="relative">
          <button
            className="text-black flex items-center justify-center w-24 h-10 hover:text-red-300"
            onClick={() => getScore()}
          >
            Get Score
          </button>
        </div>
      </div>
    </>
  );
}

export default DummyGame;
