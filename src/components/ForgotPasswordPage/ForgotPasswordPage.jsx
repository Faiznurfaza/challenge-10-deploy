"use client";

import React, { useState } from "react";
import axios from "axios";
import ForgotPasswordForm from "./ForgotPasswordForm";
import Link from "next/link";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function ForgotPasswordPage() {
  const [email, setEmail] = useState("");

  const [submit, setSubmit] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const [resetButton, setResetButton] = useState("Reset");
  const [spinner, setSpinner] = useState("");

  const onInputChange = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrorMsg("");
    try {
      setResetButton("");
      setSpinner(faSpinner);

      const res = await axios.post(
        "https://gatots-api.vercel.app/api/v1/user/forgot-password",
        { email: email }
      );

      setSpinner("");
      setResetButton("Done");
      setSubmit(true);
    } catch (err) {
      console.log(err);
      if (err.response.status === 404) {
        setSpinner("");
        setResetButton("Reset Failed");
        setErrorMsg(
          "Email address not registered, try again with registered email address"
        );
      } else if (err.response.status === 500) {
        setSpinner("");
        setResetButton("Reset Failed");
        setErrorMsg("Unexpected error happens, please try again later.");
      } else {
        setSpinner("");
        setResetButton("Reset Failed");
        setErrorMsg("Internal server error");
      }

      setTimeout(() => {
        setResetButton("Reset");
      }, 1000);
    }
  };

  function ShowResults() {
    return (
      <div style={styles.resultContainer}>
        <h4>Reset Password Code is sent to your registered email</h4>
        <Link href="/resetpassword">Reset Password Here</Link>
      </div>
    );
  }
  return (
    <div className="main">
      <ForgotPasswordForm
        email={email}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        ShowResults={ShowResults}
        submit={submit}
        resetButton={resetButton}
        Spinner={spinner}
        errorMsg={errorMsg}
      />
    </div>
  );
}

const styles = {
  resultContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default ForgotPasswordPage;
