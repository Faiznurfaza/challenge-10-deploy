"use client";

import React, { useState } from "react";
import ReactPlayer from "react-player";

export default function VideoPlayer() {
  const [play, setPlay] = useState(false);
  const [URL, setURL] = useState("");

  function handleSubmit() {
    setPlay(true);
    setURL(URL);
  }

  function StopPlay() {
    setPlay(!play);
    setURL("");
  }

  return (
    <>
      <div className="flex flex-col items-center justify-center px-6 pt-0 lg:px-8 min-h-screen">
        {play ? (
          <div className="mb-4">
            <ReactPlayer
              className="vid-player"
              url={URL}
              controls
              width="720px"
              height="360px"
              config={{
                youtube: { playerVars: { origin: "https://www.youtube.com" } },
              }}
            />
            <button
              onClick={StopPlay}
              className="px-4 py-2 mt-2 bg-red-500 text-white rounded-md"
            >
              Stop
            </button>
          </div>
        ) : null}
        <div>
          <label htmlFor="url-video" className="mb-4">
            <input
              id="url-video"
              type="text"
              value={URL}
              className="px-4 py-2 border border-gray-300 rounded-md"
              onChange={(e) => setURL(e.target.value)}
            />
            <button
              onClick={handleSubmit}
              className="px-4 py-2 ml-2 bg-blue-500 text-white rounded-md"
            >
              Play
            </button>
          </label>
          <p className="mt-2 text-sm font-light">
            Please input a valid Video URL to stream
          </p>
        </div>
      </div>
    </>
  );
}
