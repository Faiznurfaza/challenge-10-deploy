"use client";

import React, { useEffect } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import Cookies from "js-cookie";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faPlay,
  faUpload,
  faFolder,
  faCircleInfo,
} from "@fortawesome/free-solid-svg-icons";

export default function VideoInfo() {
  const router = useRouter();
  let userId = Cookies.get("userId");
  useEffect(() => {
    if (!userId) {
      router.push("/login");
    }
  }, [router, userId]);

  return (
    <div className="flex flex-col items-center justify-center px-6 pt-0 lg:px-8 min-h-screen">
      <div className="mb-20">
        <h1 className="text-2xl font-semibold">
          Videos Feature <FontAwesomeIcon icon={faCircleInfo} />
        </h1>
      </div>
      <div className="grid grid-cols-1 lg:grid-cols-3 gap-4">
        <div className="bg-white p-4 rounded-md shadow-md text-center">
          <h2 className="text-lg font-bold mb-4">Video Streaming</h2>
          <FontAwesomeIcon icon={faPlay} />
          <p className="text-sm font-semibold mb-4">
            You can now stream any video
          </p>
          <Link
            href="/video/stream"
            className="text-blue-500 hover:text-blue-800 font-semibold text-sm"
          >
            <FontAwesomeIcon icon={faArrowRight} /> Stream Here
          </Link>
        </div>
        <div className="bg-white p-4 rounded-md shadow-md text-center">
          <h2 className="text-lg font-bold mb-4">Upload Video</h2>
          <FontAwesomeIcon icon={faUpload} />
          <p className="text-sm font-semibold">
            You now can upload any video up to 3 videos per user
          </p>
          <p className="text-sm font-semibold">
            and can play your own uploaded videos
          </p>
          <Link
            href="/video/upload"
            className="text-blue-500 hover:text-blue-800 font-semibold text-sm"
          >
            <FontAwesomeIcon icon={faArrowRight} /> Upload Here
          </Link>
        </div>
        <div className="bg-white p-4 rounded-md shadow-md text-center">
          <h2 className="text-lg font-bold mb-4">User Video</h2>
          <FontAwesomeIcon icon={faFolder} className="mb-2" />
          <p className="text-sm font-semibold mb-4">
            You can manage your own uploaded videos
          </p>
          <Link
            href="/video/list"
            className="text-blue-500 hover:text-blue-800 font-semibold text-sm"
          >
            <FontAwesomeIcon icon={faArrowRight} /> Manage Here
          </Link>
        </div>
      </div>
    </div>
  );
}
