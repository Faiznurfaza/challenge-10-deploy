"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import LoginForm from "./LoginForm";
import axios from "axios";
import Cookies from "js-cookie";
import { LoginAction } from "@/redux/features/AuthReducer";
import { useAppDispatch } from "@/redux/hooks";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

export default function Login() {
  const router = useRouter();
  const dispatch = useAppDispatch()
  const isLoggedIn = Cookies.get('userId')
  if (isLoggedIn) {
    router.push("/home");
  }

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const [signInButton, setSignInButton] = useState("Sign in")
  const [spinner, setSpinner] = useState('')
  const [loginInfo, setLoginInfo] = useState("")

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setUser((prevState) => ({ ...prevState, [name]: value }));
  };

  const forgotPassword = (e) => {
    router.push("/forgotpassword");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoginInfo("")
    try {
      setSignInButton("")
      setSpinner(faSpinner)

      const res = await axios.post(
        "https://gatots-api.vercel.app/api/v1/user/login",
        user
      );
      const userId = res.data.userId;
      const expirationAge = 7;
      Cookies.set("userId", userId, { expires: expirationAge });

      setSpinner('')
      setSignInButton("Done")

      router.push("/home");
      dispatch(LoginAction())
    } catch (error) {
      
      console.log(error.response.data.msg);
      const err = error.response.data.msg
      if (err === 'Email Tidak Ditemukan') {
        setLoginInfo("Sign in failed : Wrong email or email not found !")
      } else if (err === 'Password Salah') {
        setLoginInfo("Sign in failed : Wrong password !")
      } else {
        setLoginInfo("Sign in failed : Internal Server Error")
      }

        setSpinner('')
        setSignInButton("Sign in Failed!")
      
      setTimeout(() => {
        setSignInButton("Sign in")
      }, 2000);
    }
  };

  return (
    <>
      <LoginForm
        user={user}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        forgotPassword={forgotPassword}
        signInButton={signInButton}
        Spinner={spinner}
        loginInfo={loginInfo}
      />
    </>
  );
}
