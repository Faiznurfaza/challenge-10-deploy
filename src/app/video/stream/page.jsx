import React from 'react';
import VideoPlayer from '@/components/VideoPage/VideoPlayer';
import Navbar from '@/components/Navbar/Navbar';
import FooterSection from '@/components/LandingPage/FooterSection';

function VideoPage() {
    return (
        <>
            <Navbar/>
            <VideoPlayer/>
            <FooterSection/>
        </>
    )
}

export default VideoPage