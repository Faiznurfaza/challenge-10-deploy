import React from 'react';
import VideoList from '@/components/VideoPage/VideoList';
import Navbar from '@/components/Navbar/Navbar';
import FooterSection from '@/components/LandingPage/FooterSection';

function VideoPage() {
    return (
        <>
            <Navbar/>
            <VideoList/>
            <FooterSection/>
        </>
    )
}

export default VideoPage