import React from 'react';
import VideoInfo from '@/components/VideoPage/VideoInfo';
import Navbar from '@/components/Navbar/Navbar';
import FooterSection from '@/components/LandingPage/FooterSection';

function VideoPage() {
    return (
        <>
            <Navbar/>
            <VideoInfo/>
            <FooterSection/>
        </>
    )
}

export default VideoPage