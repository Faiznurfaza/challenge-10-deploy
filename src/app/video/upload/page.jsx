import React from 'react';
import VideoUpload from '@/components/VideoPage/VideoUpload';
import Navbar from '@/components/Navbar/Navbar';
import FooterSection from '@/components/LandingPage/FooterSection';

function VideoPage() {
    return (
        <>
            <Navbar/>
            <VideoUpload/>
            <FooterSection/>
        </>
    )
}

export default VideoPage