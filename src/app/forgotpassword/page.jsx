/* eslint-disable react-hooks/rules-of-hooks */
import ForgotPasswordPage from "@/components/ForgotPasswordPage/ForgotPasswordPage";
import Navbar from "@/components/Navbar/Navbar";

export default function ForgotPassword() {
  return (
    <>
      <Navbar />
      <ForgotPasswordPage />
    </>
  );
}
