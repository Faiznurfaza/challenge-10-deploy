/* eslint-disable react-hooks/rules-of-hooks */
import Login from "@/components/Login/Login";
import Navbar from "@/components/Navbar/Navbar";

export default function LoginPage() {
  return (
    <>
      <Navbar />
      <Login />
    </>
  );
}
