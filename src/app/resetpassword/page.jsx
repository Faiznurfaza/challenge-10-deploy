/* eslint-disable react-hooks/rules-of-hooks */
import Navbar from "@/components/Navbar/Navbar";
import ResetPasswordPage from "@/components/ResetPasswordPage/ResetPasswordPage";

export default function ResetPassword() {
  return (
    <>
      <Navbar />
      <ResetPasswordPage />
    </>
  );
}
