import Navbar from "@/components/Navbar/Navbar";
import HomePage from "@/components/HomePage/HomePage";

export default function homePage() {
  return (
    <>
      <Navbar />
      <HomePage />
    </>
  );
}
